package learn.it.learnserver;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@Slf4j
@SpringBootTest
@ActiveProfiles("h2")
class LearnServerApplicationTests {

	@Test
	void contextLoads() {
		log.error("XXX");
	}

}
